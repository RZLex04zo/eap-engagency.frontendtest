# As Per my written notes

#### Thought it would be fun to include

1. Received email from Matthew at exactly 10:02 AM 08/14/201

Started by generating a user account inside of BitBucket using my private email address

After that I downloaded sourcetree, always wanted to try out a graphical client for git, since previous exposure to the ones in github were not to my liking.
I would just use the cli.

Configuration proved easy.


- - -

2. After setting my development environment, left the house for approximately 2-1 hours to run a small errand. 

I read the requirements beforehand. For this I used diffused thinking. Basically, a thinking method in which I leave the idea in the back of my mind. It lets me
stage the process better and focus on the things I need to get done. Specially with frontend. I am always so paranoid with frontend.

- - -

3. Got home at around 1:30 P.M

I needed to get something to eat, but before some excercise. Oxygen helps the brain afterall.

Started working on the navigation items. Decided to use a seemingly unknown css framework for the navigation and some of the other components. I like writting my code from scratch.

It is not necessarily a requirement for me. But I am still not up to date with all the bootstrap updated to version 5.1 Initial tests proved too much work. So I went with a more unobtrusive framework in w3css.

- - -

4. Finished the rest of the layout and all the components at 05:12PM after some experimentation

Had to leave the house to pick up my daughter. Diffused mode again in regards to the media queries necessary to bring the project to completion.

Got something to eat.

- - -

5. Started getting paranoid with pixel perfect design. Added some changes that I liked more in terms of the breakpoints. 

The breakpoints that I added seem to work well thus far. I am never happy with them. 

Time is currently: 07:30 PM

- - -

6. Play some bass, relax

- - -

7. Got back to Matt to let him know that I was almost done. 

Decided to take my time with it since I am having fun. Still, not happy with some of the queries.

- - -

8. 11:00 PM

Ok time to learn how to stage items in bitbucket and create branches.

- - -

9. 11:15 PM

Done with number 8. Pretty good docs. 

- - -

10. Ensure that HTML is as correct as I can possibly make it be 1:02 AM

Trying to adhere by the standards as much as possible. I normally rely on the code reviews from my team. Paranoid again.

The *anxiety* is real.

- - -

11.  Code cleanup. Prepare for a pull request

It is currently 02:04 AM

- - -
12. Final Commit and pull request

02:08 AM time to do the final commit and then the pull request!


## Pending Items

- [x] be happy with media queries. I approximated as much as I could.

