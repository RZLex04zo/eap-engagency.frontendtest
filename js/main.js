/*
|--------------------------------------------------------------------------
| Hamburger Icon Event Handler
|--------------------------------------------------------------------------
| 😬 Shamelessly taken from the w3.css reference 😬
*/
function toggleDropdownButton() {
  const dropdown_icon = document.getElementById(
    "secondary_navigation__dropdown"
  );
  if (dropdown_icon.className.indexOf("w3-show") == -1) {
    dropdown_icon.className += " w3-show";
  } else {
    dropdown_icon.className = dropdown_icon.className.replace(" w3-show", "");
  }
}
